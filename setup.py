from distutils.core import setup

setup(
    name='eth2-commons',
    version='0.0.1',
    packages=['eth2', 'eth2.commons', 'eth2.commons.resources'],
    data_files=[('eth2/commons/resources', ['eth2/commons/resources/oui.csv'])],
    url='https://bitbucket.org/eth2/commons-python/',
    license='MIT',
    author='eth2 networks',
    author_email='m.reening@eth2.link',
    description='Utilities for networking'
)
