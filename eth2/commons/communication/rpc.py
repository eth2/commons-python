from eth2.commons.communication.packets import RpcCallPacket, RpcResponsePacket
import json


class RpcHandler:
    def __init__(self, send_packet):
        self._methods = {}
        self._next_id = 1
        self._callbacks = {}
        self.send_packet = send_packet

    def add_handler(self, name, method):
        print "Registered RPC method " + name
        self._methods[name] = method

    def call(self, method_name, **kwargs):
        if 'callback' in kwargs:
            rpcid = self._next_id
            self._next_id += 1
            self._callbacks[rpcid] = kwargs['callback']
            del kwargs['callback']
        else:
            rpcid = 0
        self.send_packet(RpcCallPacket(method_name, rpcid, json.dumps(kwargs)))

    def handle_packet(self, packet):
        if isinstance(packet, RpcResponsePacket):
            callback = self._callbacks[packet.rpcid]
            if callback is not None:
                callback(**json.loads(packet.response))
                self._callbacks[packet.rpcid] = None

        elif isinstance(packet, RpcCallPacket):
            if packet.method in self._methods:
                try:
                    response = self._methods[packet.method](**json.loads(packet.args))
                    if packet.rpcid > 0:
                        self.send_packet(RpcResponsePacket(packet.rpcid, json.dumps(response)))
                except TypeError as e:
                    print "Error while handling RPC call"
                    print e
                    print "Method: " + packet.method
                    print "Args: " + packet.args
            else:
                print "Trying to call unknown RPC method " + packet.method + " with args " + packet.args

        else:
            raise ValueError("Expected RpcResponsePacket or RpcCallPacket")

