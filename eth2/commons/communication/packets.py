import json
import struct

PACKET_AUTHENTICATE = 0x01
PACKET_AUTHENTICATE_RESPONSE = 0x02
PACKET_RPC_CALL = 0x03
PACKET_RPC_RESPONSE = 0x04
PACKET_PUBLISH = 0x05
PACKET_SUBSCRIBE = 0x06
PACKET_PING = 0x07
PACKET_PONG = 0x08


class Packet(object):
    def encode(self):
        payload = self._encode()
        packet = b""
        packet += struct.pack(">B", self.__packet_id__)
        packet += struct.pack(">I", len(payload))
        packet += payload
        return packet

    def _encode(self):
        raise NotImplementedError


class AuthenticatePacket(Packet):
    __packet_id__ = PACKET_AUTHENTICATE

    def __init__(self, module_type, module_token):
        self.type = module_type
        self.token = module_token

    def __repr__(self):
        return '<AuthenticatePacket (type={}, token={})>'.format(self.type, self.token)

    def _encode(self):
        has_token = len(self.token) > 0

        payload = b""
        payload += struct.pack(">H", len(self.type))
        payload += self.type.encode("utf-8")
        payload += struct.pack(">B", has_token)  # Send 0x1 when we have a token and 0x0 when we don't
        if has_token:
            payload += struct.pack(">H", len(self.token))
            payload += self.token.encode("utf-8")

        return payload


class AuthenticateResponsePacket(Packet):
    __packet_id__ = PACKET_AUTHENTICATE_RESPONSE

    def __init__(self, ok, new_token):
        self.ok = ok
        self.new_token = new_token

    def __repr__(self):
        return '<AuthenticateResponsePacket (ok={}, new_token={})>'.format(self.ok, self.new_token)

    def _encode(self):
        payload = b""
        payload += struct.pack(">B", self.ok)
        payload += struct.pack(">B", self.new_token is not None)
        if self.new_token is not None:
            payload += struct.pack(">H", len(self.new_token))
            payload += self.new_token.encode("utf-8")

        return payload


class RpcCallPacket(Packet):
    __packet_id__ = PACKET_RPC_CALL

    def __init__(self, method, rpcid, args):
        self.method = method
        self.rpcid = rpcid
        self.args = args

    def __repr__(self):
        return '<RpcCallPacket (method={}, rpcid={}, args={})>'.format(self.method, self.rpcid, self.args)

    def _encode(self):
        payload = b""

        payload += struct.pack(">H", len(self.method))
        payload += self.method.encode("utf-8")
        payload += struct.pack(">I", self.rpcid)
        payload += struct.pack(">I", len(self.args))
        payload += self.args.encode("utf-8")

        return payload


class RpcResponsePacket(Packet):
    __packet_id__ = PACKET_RPC_RESPONSE

    def __init__(self, rpcid, response):
        self.rpcid = rpcid
        self.response = response
        if self.response is None:
            self.response = "{}"

    def __repr__(self):
        return '<RpcResponsePacket (rpcid={}, response={})>'.format(self.rpcid, self.response)

    def _encode(self):
        payload = b""

        payload += struct.pack(">I", self.rpcid)
        payload += struct.pack(">I", len(self.response))
        payload += self.response.encode("utf-8")

        return payload


class PublishPacket(Packet):
    __packet_id__ = PACKET_PUBLISH

    def __init__(self, topic, payload):
        self.topic = topic
        self.payload = payload

    def __repr__(self):
        return '<PublishPacket (topic={}, payload={})>'.format(self.topic, self.payload)

    def _encode(self):
        payload = b""

        payload += struct.pack(">H", len(self.topic))
        payload += self.topic.encode("utf-8")
        payload += struct.pack(">I", len(self.payload))
        payload += self.payload.encode("utf-8")

        return payload


class SubscribePacket(Packet):
    __packet_id__ = PACKET_SUBSCRIBE

    def __init__(self, topic):
        self.topic = topic

    def __repr__(self):
        return '<SubscribePacket (topic={})>'.format(self.topic)

    def _encode(self):
        payload = b""

        payload += struct.pack(">H", len(self.topic))
        payload += self.topic.encode("utf-8")

        return payload


class PingPacket(Packet):
    __packet_id__ = PACKET_PING

    def __repr__(self):
        return '<PingPacket ()>'

    def _encode(self):
        return b""


class PongPacket(Packet):
    __packet_id__ = PACKET_PONG

    def __repr__(self):
        return '<PongPacket ()>'

    def _encode(self):
        return b""


# You can call this method when the first 5 bytes of the packet are received
# We will be able to determine which packet it is after that and request the remaining bytes
# The `more_data_callback` function should accept a number (length) and a callback which it will call
# when the remaining data is received
def parse_packet(data, more_data_callback, done_callback):
    packet_type = struct.unpack(">B", data[0:1])[0]
    packet_length = struct.unpack(">I", data[1:5])[0]

    def on_data_received(data):
        if len(data) != packet_length:
            print "Chunk was not of requested length"
            print "Requested " + str(packet_length)
            print "Got " + str(len(data))
            print "Decoding may fail"

        if packet_type == PACKET_AUTHENTICATE:
            _parse_authenticate(data, done_callback)
        elif packet_type == PACKET_AUTHENTICATE_RESPONSE:
            _parse_authenticate_response(data, done_callback)
        elif packet_type == PACKET_RPC_CALL:
            _parse_rpc_call(data, done_callback)
        elif packet_type == PACKET_RPC_RESPONSE:
            _parse_rpc_response(data, done_callback)
        elif packet_type == PACKET_PUBLISH:
            _parse_publish(data, done_callback)
        elif packet_type == PACKET_SUBSCRIBE:
            _parse_subscribe(data, done_callback)
        elif packet_type == PACKET_PING:
            done_callback(PingPacket())
        elif packet_type == PACKET_PONG:
            done_callback(PongPacket())
        else:
            print "Unknown packet type " + str(packet_type)
            print data.encode("hex")
        pass

    more_data_callback(packet_length, on_data_received)


def _parse_authenticate(data, callback):
    type_length = struct.unpack(">H", data[0:2])[0]
    type = data[2:2 + type_length].decode("utf-8")
    i = 2 + type_length
    has_token = struct.unpack(">B", data[i:i + 1])[0] > 0
    i += 1

    if has_token:
        token_length = struct.unpack(">H", data[i:i + 2])[0]
        i += 2
        token = data[i:i + token_length].decode("utf-8")
    else:
        token = None

    callback(AuthenticatePacket(type, token))


def _parse_authenticate_response(data, callback):
    was_ok = struct.unpack(">B", data[0:1])[0]
    has_new_token = struct.unpack(">B", data[1:2])[0]

    if has_new_token:
        new_token_len = struct.unpack(">H", data[2:4])[0]
        new_token = data[4:4 + new_token_len].decode("utf-8")
    else:
        new_token = None

    callback(AuthenticateResponsePacket(was_ok, new_token))


def _parse_rpc_call(data, callback):
    method_len = struct.unpack(">H", data[0:2])[0]
    method = data[2:2 + method_len].decode("utf-8")

    i = 2 + method_len

    rpcid = struct.unpack(">I", data[i:i + 4])[0]
    i += 4

    args_length = struct.unpack(">I", data[i:i + 4])[0]
    args = data[i+4: i+4+args_length].decode("utf-8")

    callback(RpcCallPacket(method, rpcid, args))


def _parse_rpc_response(data, callback):
    rpcid = struct.unpack(">I", data[0:4])[0]

    args_length = struct.unpack(">I", data[4:8])[0]
    args = data[8:8 + args_length].decode("utf-8")

    callback(RpcResponsePacket(rpcid, args))


def _parse_publish(data, callback):
    pass


def _parse_subscribe(data, callback):
    pass
