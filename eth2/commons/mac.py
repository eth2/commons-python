import csv
from re import findall, match, split, I
from struct import pack, unpack


class MACAddress(object):
    def __init__(self, address=None):
        if not address:
            address = 6 * [0]

        if isinstance(address, (tuple, list,)):
            address = filter(lambda i: 0 <= int(i) <= 255, address)

            if len(address) != 6:
                raise Exception('Invalid MAC address')

            self._address = address
            return

        if match('[0-9a-f]{12}', address, I):
            octets = findall('..', address)
        elif match('([0-9a-f]{2}[-:]){5}[0-9a-f]{2}', address, I):
            octets = split('[-:]', address)
        elif match('([0-9a-f]{4}\.?){2}[0-9a-f]{4}', address, I):
            octets = findall('..', ''.join(address.split('.')))
        else:
            raise Exception('Invalid MAC address')

        self._address = map(lambda i: int(i, 16), octets)

    def is_multicast(self):
        return bool(self._address[0] & 1)
    
    def is_unicast(self):
        return not self.is_multicast()

    def is_local_admin(self):
        return bool(self._address[0] & 2)

    def get_unicast(self):
        addr = MACAddress()
        addr._address = list(self._address)
        addr._address[0] &= 0b11111110
        return addr

    def get_multicast(self):
        addr = MACAddress()
        addr._address = list(self._address)
        addr._address[0] |= 0b00000001
        return addr

    def pack(self):
        return pack('BBBBBB', *self._address)

    @staticmethod
    def unpack(string):
        return MACAddress(unpack('BBBBBB', string))

    def __str__(self, delimiter=':'):
        string = delimiter.join(6 * ['{:02x}'])
        return string.format(*self._address)

    def __repr__(self):
        return '<MACAddress ({})>'.format(str(self))

class OUIDatabase(object):
    def __init__(self):
        filename = __file__.rsplit('/', 1)[0] + '/resources/oui.csv'
        self._vendors = {}

        with open(filename, 'r') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                self._vendors[row[0].lower()] = row[1]

    def get_manufacturer(self, addr):
        if isinstance(addr, str):
            addr = MACAddress(addr)

        oui = str(addr).replace(':', '')[:6]

        if not oui in self._vendors:
            return None

        return self._vendors[oui]

    def search(self, term):
        results = []
        for oui, man in self._vendors.iteritems():
            if term.lower() in man.lower():
                results.append((oui, man,))

        return results

